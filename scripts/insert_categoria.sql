﻿insert into public."Categoria_dim"
("Id", 
  "Codigo", 
  "Categoria")
SELECT distinct
  o.categoria_id, 
  o.categoria_codigo, 
  replace(o.categoria, ';', ',')
FROM 
  public.origen_convocatoria o
where o.categoria_id not in (select "Categoria_dim"."Id" 
						  from public."Categoria_dim")
order by 1 asc;
