﻿create or replace view v_nombre_contacto (nombre_contacto)
as
SELECT DISTINCT
	trim(replace(
		trim(replace(
			upper(trim(o.nombre_contacto)), ':', ''
		)), '.', ''
	))
FROM 
  public.origen_convocatoria o
order by 1;

-- 5492 inicial
-- con trim y upper 3988
-- con replace : y trim 3987
-- con replace . y trim 3789