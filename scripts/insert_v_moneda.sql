﻿truncate public.v_moneda_dim;
insert into public.v_moneda_dim
("Moneda", 
  "Codigo")
SELECT distinct
 op.moneda,
 op.moneda_codigo  
FROM 
  public.origen_convocatoria op
  order by 1;	