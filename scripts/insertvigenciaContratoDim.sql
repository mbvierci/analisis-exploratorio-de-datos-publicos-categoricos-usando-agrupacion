﻿CREATE TABLE v_vigencia_contrato_dim
(
  vigencia_contrato character varying(100),
  valor_vigencia double precision
);

﻿COPY v_vigencia_contrato_dim FROM '/home/belen/tesis/test.csv' DELIMITER ',' CSV;

drop sequence vigencia_contrato_seq;
CREATE SEQUENCE vigencia_contrato_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 18850
  CACHE 1;

CREATE TABLE "Vigencia_Contrato_dim"
(
  "Id" integer NOT NULL,
  vigencia_contrato character varying(100),
  valor_vigencia double precision
  CONSTRAINT "Id_tipo_modificacion_pk" PRIMARY KEY ("Id" )
);

CREATE OR REPLACE VIEW v_vigencia_contrato AS 
	SELECT DISTINCT	v.vigencia_contrato 	as vigencia_contrato,
			v.valor_vigencia 	as valor_vigencia
	FROM v_vigencia_contrato_dim v
	ORDER BY 2;

truncate table public."Vigencia_Contrato_dim";
insert into public."Vigencia_Contrato_dim"
( "Id", vigencia_contrato, valor_vigencia)
SELECT
  nextval('vigencia_contrato_seq') as id, 
  v.vigencia_contrato,
  v.valor_vigencia
FROM 
  v_vigencia_contrato v
WHERE v.vigencia_contrato not in (select o.vigencia_contrato
			 from public."Vigencia_Contrato_dim" o);