﻿truncate public.v_convocante_dim;

insert into public.v_convocante_dim
("Convocante", 
  "Padre", 
  "Abuelo")
SELECT
distinct
 trim(substring(op.convocante from '([^/]*).*')) as hijo,
 trim(substring(op.convocante from '\/(.+)\/')) as padre,
 trim(substring((substring(op.convocante from '\/(.+)')) from '\/(.+)')) as abuelo
FROM 
  public.origen_convocatoria op
  order by 1;


