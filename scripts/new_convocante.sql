﻿create or replace view v_convocante as
SELECT distinct
	op.convocante					as convocante,
	trim(substring(op.convocante 
		       from '([^/]*).*')) 		as hijo,
	trim(substring(op.convocante 
		       from '\/(.+)\/')) 		as padre,
	trim(substring((substring(op.convocante 
				  from '\/(.+)')) 
		        from '\/(.+)')) 		as abuelo
FROM contrato op
order by 1;

--TABLA CONVOCANTE
DROP TABLE convocante_dim;
CREATE TABLE convocante_dim
(
  id integer NOT NULL,
  hijo character varying(200),
  padre character varying(100),
  abuelo character varying(100),
  convocante character varying(200),
  CONSTRAINT id_convocante_pk PRIMARY KEY (id)
)

truncate convocante_dim;

--SI EL CONVOCANTE YA EXISTE EN LA DIMENSION Y EL NUEVO TIENE PADRE Y/O ABUELO
UPDATE public.convocante_dim c SET
	padre 	= v.padre,
	abuelo 	= v.abuelo
from public.v_convocante v
where v.hijo = c.hijo
and v.padre is not null
and v.abuelo is not null;

--SI EL CONVOCANTE NO EXISTE EN LA DIMENSION
insert into public.convocante_dim
( id, 
  hijo, 
  padre, 
  abuelo,
  convocante)
select
  nextval('convocante_seq') 											as id,
  v.hijo 													as hijo,
  case  when v.padre is not null then v.padre
	when v.padre is null then
		case when v.hijo in (select distinct vc.padre
					  from public.v_convocante vc
					  where vc.padre is not null) then (select max(vc.padre)
									      from public.v_convocante vc
									      where vc.padre = v.hijo)
		when v.hijo in (select distinct vc.abuelo
				      from public.v_convocante vc
				      where vc.abuelo is not null) then (select vc.abuelo
									   from public.v_convocante vc
									   where vc.abuelo = v.hijo)
		else 'Desconocido'
		end
	else 'Desconocido'
  end 														as padre,
  case when v.abuelo is not null then v.abuelo
       when v.abuelo is null then
		case when v.hijo in (select distinct vc.padre
					  from public.v_convocante vc
					  where vc.padre is not null) then (select max(vc.abuelo)
									      from public.v_convocante vc
									      where vc.padre = v.hijo)
		when v.hijo in (select distinct vc.abuelo
				      from public.v_convocante vc
				      where vc.abuelo is not null) then (select vc.abuelo
									   from public.v_convocante vc
									   where vc.abuelo = v.hijo)
		else 'Desconocido'
		end
	else 'Desconocido'
  end 														as abuelo,
  v.convocante													as convocante
from public.v_convocante v
where v.convocante not in (select convocante_dim.convocante
			   from public.convocante_dim);