CREATE TABLE v_tipo_modificacion_dim
(
  "Tipo" character varying(200)
);

﻿COPY v_provedor_dim FROM '/home/belen/tesis/test.csv' DELIMITER ',' CSV;

CREATE SEQUENCE tipo_modificacion_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 18850
  CACHE 1;

CREATE TABLE "Tipo_Modificacion_dim"
(
  "Id" integer NOT NULL,
  "Tipo" character varying(50),
  CONSTRAINT "Id_tipo_modificacion_pk" PRIMARY KEY ("Id" )
);

insert into public."Tipo_Modificacion_dim"
( "Id", "Tipo")
SELECT
  nextval('tipo_modificacion_seq') as id, 
  v.tipo
FROM 
  v_tipo_modificacion_dim v
WHERE v."Tipo" not in (select o."Tipo"
			 from public."Tipo_Modificacion_dim" o);
