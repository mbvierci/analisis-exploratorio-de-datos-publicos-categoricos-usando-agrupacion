﻿--drop sequence estado_seq;
CREATE SEQUENCE plazo_entrega_adquisicion_seq START 1;

insert into public.plazo_entrega_adquisicion_dim
( id,
  plazo_entrega_adquisicion, 
  periodo)
SELECT
  nextval('plazo_entrega_adquisicion_seq') as id, 
  v.plazo_entrega_adquisicion, 
  CASE
	WHEN v.plazo_entrega_adquisicion like '%DIA%' 		THEN 'DÍA'
	WHEN v.plazo_entrega_adquisicion like '%DÍA%' 		THEN 'DÍA'
	WHEN v.plazo_entrega_adquisicion like '%MES%' 		THEN 'MES'
	WHEN v.plazo_entrega_adquisicion like '%AÑO%' 		THEN 'AÑO'
	WHEN v.plazo_entrega_adquisicion like '%SEMANA%'	THEN 'SEMANA'
	WHEN v.plazo_entrega_adquisicion like '%HORA%'		THEN 'HORA'
	WHEN v.plazo_entrega_adquisicion like '%HS%'		THEN 'HORA'
	ELSE 'No aplica'
  END as periodo
FROM 
  public.v_plazo_entrega_adquisicion v
where v.plazo_entrega_adquisicion not in (select p.plazo_entrega_adquisicion
					  from public.plazo_entrega_adquisicion_dim p);