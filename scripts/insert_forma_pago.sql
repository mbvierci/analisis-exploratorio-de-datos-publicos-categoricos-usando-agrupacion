﻿--drop sequence estado_seq;
CREATE SEQUENCE forma_pago_seq START 1;

insert into public.forma_pago_dim
( id,
  codigo, 
  forma_pago)
SELECT
  nextval('forma_pago_seq') as id, 
  v.codigo, 
  v.forma_pago
FROM 
  public.v_forma_pago_dim v
where v.codigo not in (select s.codigo
			 from public.forma_pago_dim s);