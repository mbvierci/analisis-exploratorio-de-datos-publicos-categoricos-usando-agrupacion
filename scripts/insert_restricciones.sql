﻿--drop sequence estado_seq;
CREATE SEQUENCE restriccion_seq START 1;

insert into public.restriccion
( id,
  restriccion)
SELECT
  nextval('restriccion_seq') as id, 
  v.restriccion
FROM 
  public.v_restriccion v
where v.restriccion not in (select r.restriccion
			 from public.restriccion r);