CREATE TABLE v_provedor_dim
(
  ruc character varying(20),
  razon_social character varying(200)
);

COPY v_provedor_dim FROM '/home/belen/tesis/test.csv' DELIMITER ',' CSV;

CREATE SEQUENCE proveedor_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 18850
  CACHE 1;
ALTER TABLE proveedor_seq;
  
CREATE TABLE "Proveedor_dim"
(
  "Id" integer NOT NULL,
  "Ruc" character varying(50),
  razon_social character varying(200),
  CONSTRAINT "Id_proveedor_pk" PRIMARY KEY ("Id" )
);
  
insert into public."Proveedor_dim"
( "Id", "Ruc", razon_social)
SELECT
  nextval('proveedor_seq') as id, 
  v.ruc, 
  v.razon_social
FROM 
  v_provedor_dim v
WHERE v.ruc not in (select o."Ruc"
			 from public."Proveedor_dim" o);
