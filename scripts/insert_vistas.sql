﻿--V_CONVOCANTE
truncate public.v_convocante_dim;
insert into public.v_convocante_dim
("Convocante", 
  "Padre", 
  "Abuelo")
SELECT
distinct
 trim(substring(op.convocante from '([^/]*).*')) as hijo,
 trim(substring(op.convocante from '\/(.+)\/')) as padre,
 trim(substring((substring(op.convocante from '\/(.+)')) from '\/(.+)')) as abuelo
FROM 
  public."Origen_planificacion" op
  order by 1;

--V_ESTADO
truncate public.v_estado_dim;
insert into public.v_estado_dim
("Estado", 
  "Codigo")
SELECT distinct
 op.estado,
 op.estado_codigo  
FROM 
  public."Origen_planificacion" op
  order by 1;

--V_MONEDA
truncate public.v_moneda_dim;
insert into public.v_moneda_dim
("Moneda", 
  "Codigo")
SELECT distinct
 op.moneda,
 op.moneda_codigo  
FROM 
  public."Origen_planificacion" op
  order by 1;

--V_OBJETO_LICITACION
truncate public.v_objeto_licitacion_dim;
insert into public.v_objeto_licitacion_dim
("Objeto_licitacion", 
  "Codigo")
SELECT distinct
 op.objeto_licitacion,
 op.objeto_licitacion_codigo  
FROM 
  public."Origen_planificacion" op
  order by 1;	