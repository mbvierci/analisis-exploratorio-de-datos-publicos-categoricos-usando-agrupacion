﻿insert into public.organismo_financiador_dim
(	id, 
	organismo_financiador)
SELECT distinct
  case	when o.organismo_financiador_id is not null 
		then o.organismo_financiador_id
	else -2
  end,
  case	when o.organismo_financiador in ('-2', '')
		then 'Desconocido'
	else o.organismo_financiador 
  end
FROM 
  public.origen_convocatoria o
where o.categoria_id not in (select of.id 
			     from public.organismo_financiador_dim of)
order by 1 asc;