﻿--drop sequence convocante_seq;
--CREATE SEQUENCE convocante_seq START 1;

--SI EL CONVOCANTE YA EXISTE EN LA DIMENSION Y EL NUEVO TIENE PADRE Y/O ABUELO
UPDATE public."Convocante_dim" c SET
	"Padre" = 	v."Padre",
	"Abuelo" = 	v."Abuelo"
from public.v_convocante_dim v
where v."Convocante" = c."Convocante"
and v."Padre" is not null
and v."Abuelo" is not null;

--SI EL CONVOCANTE NO EXISTE EN LA DIMENSION
insert into public."Convocante_dim"
( "Id", 
  "Convocante", 
  "Padre", 
  "Abuelo")
select
  nextval('convocante_seq') as id,
  v."Convocante" as convocante,
  case  when v."Padre" is not null then v."Padre"
	when v."Padre" is null then
		case when v."Convocante" in (select distinct vc."Padre"
					  from public.v_convocante_dim vc
					  where vc."Padre" is not null) then (select max(vc."Padre")
									      from public.v_convocante_dim vc
									      where vc."Padre" = v."Convocante")
		when v."Convocante" in (select distinct vc."Abuelo"
				      from public.v_convocante_dim vc
				      where vc."Abuelo" is not null) then (select vc."Abuelo"
									   from public.v_convocante_dim vc
									   where vc."Abuelo" = v."Convocante")
		else 'Desconocido'
		end
	else 'Desconocido'
  end as padre,
  case when v."Abuelo" is not null then v."Abuelo"
       when v."Abuelo" is null then
		case when v."Convocante" in (select distinct vc."Padre"
					  from public.v_convocante_dim vc
					  where vc."Padre" is not null) then (select max(vc."Abuelo")
									      from public.v_convocante_dim vc
									      where vc."Padre" = v."Convocante")
		when v."Convocante" in (select distinct vc."Abuelo"
				      from public.v_convocante_dim vc
				      where vc."Abuelo" is not null) then (select vc."Abuelo"
									   from public.v_convocante_dim vc
									   where vc."Abuelo" = v."Convocante")
		else 'Desconocido'
		end
	else 'Desconocido'
  end as abuelo
from public.v_convocante_dim v
where v."Convocante" not in (select "Convocante_dim"."Convocante"
			     from public."Convocante_dim");
