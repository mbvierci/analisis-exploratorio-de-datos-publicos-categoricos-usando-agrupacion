﻿/** VIGENCIA CONTRATOS **/
--cree una vista auxiliar de vigencia de contrato donde hace un insert solo de los distintos
CREATE OR REPLACE VIEW v_vigencia_contrato AS 
	SELECT DISTINCT	v.vigencia_contrato 	as vigencia_contrato,
			v.valor_vigencia 	as valor_vigencia
	FROM v_vigencia_contrato_dim v
	ORDER BY 2;

--modifique la carga de la dimension para que apunte a la vista que tiene los distintos
truncate table public."Vigencia_Contrato_dim";
insert into public."Vigencia_Contrato_dim"
( "Id", vigencia_contrato, valor_vigencia)
SELECT
  nextval('vigencia_contrato_seq') as id, 
  v.vigencia_contrato,
  v.valor_vigencia
FROM 
  v_vigencia_contrato v
WHERE v.vigencia_contrato not in (select o.vigencia_contrato
			 from public."Vigencia_Contrato_dim" o);
/** FIN VIGENCIA CONTRATOS **/

drop view v_convocatoria cascade;
/** VISTA DE CONTRATOS ADJUDICADOS **/
create or replace view v_convocatoria as
select 	oc.id_llamado			as id_llamado
	,oc.tipo_garantia_oferta	as tipo_garantia_oferta
	,oc.sistema_adjudicacion	as sistema_adjudicacion
	,oc.organismo_financiador	as organismo_financiador
	,oc.metodo_seleccion		as metodo_seleccion
	,oc.forma_pago			as forma_pago
	,oc.categoria			as categoria
	,oc.tipo_procedimiento		as tipo_procedimiento
	,oc.fecha_publicacion		as fecha_publicacion
	,oc.fecha_tope_respuesta	as fecha_tope_respuesta
	,oc.fecha_entrega_oferta	as fecha_entrega_oferta
	,oc.fecha_cierre_propuesta	as fecha_cierre_propuesta
	,oc.convocante			as convocante
from origen_convocatoria oc
where oc.estado = 'Adjudicado';

/** VISTA DE PROVEEDORES **/
create or replace view v_proveedor as
select 	max(trim(upper(replace(p.razon_social, '', ''))))	as razon_social
	,case when p."Ruc" is null
	      then max(trim(upper(p.razon_social)))
	 else p."Ruc" end					as ruc
from "Proveedor_dim" p
group by p."Ruc"
order by 1;

-- Table: ranking_set
-- DROP TABLE ranking_set;
CREATE TABLE ranking_set
(
  id		integer	NOT NULL,
  ruc 		integer,
  razon_social	character varying(200),
  aporte_neto 	integer,
  CONSTRAINT 	ranking_set_pk	PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

--TMP DE SIMILITUD DE CONTRATO
drop table similitud_contrato_modificado_fct_tmp;
CREATE TABLE similitud_contrato_modificado_fct_tmp
(
  id_contrato				character varying(500),
  tipo_garantia_oferta 			character varying(500),
  sistema_adjudicacion 			character varying(500),
  objeto_licitacion 			character varying(500),
  forma_pago 				character varying(500),
  categoria 				character varying(500),
  tipo_procedimiento 			character varying(500),
  vigencia_contrato			character varying(500),
  dpto_proveedor 			character varying(500),
  anho_firma				character varying(500),
  anho_elec_gral_firma			character varying(500),
  anho_elec_mun_firma			character varying(500),
  trimestre_firma			character varying(500),
  mes_firma				character varying(500),
  anho_publicacion			character varying(500),
  anho_elec_gral_publicacion		character varying(500),
  anho_elec_mun_publicacion		character varying(500),
  trimestre_publicacion			character varying(500),
  mes_publicacion			character varying(500),
  periodo_public_rpta			character varying(500),
  convocante				character varying(500),
  monto					character varying(500),
  modificado 				character varying(500)
)
WITH (
  OIDS=FALSE
);

--CARGA DE SIMILITUD DE CONTRATO
truncate table similitud_contrato_modificado_fct_tmp;
INSERT INTO similitud_contrato_modificado_fct_tmp( id_contrato,
            tipo_garantia_oferta, sistema_adjudicacion, objeto_licitacion, forma_pago, categoria, tipo_procedimiento, vigencia_contrato,
            dpto_proveedor, anho_firma, anho_elec_gral_firma, anho_elec_mun_firma, trimestre_firma, mes_firma, anho_publicacion, anho_elec_gral_publicacion, anho_elec_mun_publicacion, trimestre_publicacion,
            mes_publicacion, periodo_public_rpta, convocante, monto, modificado)
            select ct.id_contrato,
		   coalesce(cv.tipo_garantia_oferta, 'Desconocido') 				as tipo_garantia_oferta,
		   coalesce(cv.sistema_adjudicacion, 'Desconocido') 				as sistema_adjudicacion,
	           coalesce(op.objeto_licitacion, 'Desconocido')				as objeto_licitacion,
	           coalesce(cv.forma_pago, 'Desconocido')					as forma_pago,
	           coalesce(replace(cv.categoria, ';', ','), 'Desconocido')			as categoria,
	           coalesce(trim(cv.tipo_procedimiento), 'Desconocido')				as tipo_procedimiento,
	           case	when vc.valor_vigencia > 365 					
				then 'Mayor a 1 anho'
			when vc.valor_vigencia <= 365 and vc.valor_vigencia > 270 	
				then 'Entre 9 meses y 1 anho'
			when vc.valor_vigencia <= 270 and vc.valor_vigencia > 180 	
				then 'Entre 6 y 9 meses'
			when vc.valor_vigencia <= 180 and vc.valor_vigencia > 90 	
				then 'Entre 3 y 6 meses'
			when vc.valor_vigencia <= 90  and vc.valor_vigencia <> -2   
				then 'Menor a 3 meses'
			else 'Desconocido'
		   end 												as vigencia_contrato,
	           coalesce((trim(p.departamento)), 'Desconocido')						as dpto_proveedor,
	           case when f.anho = 2090
				then 'Desconocido'
			else coalesce((trim(to_char(f.anho, '9999'))), 'Desconocido')	
		   end												as anho_firma,
		   coalesce((trim(to_char(f.anho_elec_gral, '9999'))), 'Desconocido')				as anho_elec_gral_firma,
		   coalesce((trim(to_char(f.anho_elec_mun, '9999'))), 'Desconocido')				as anho_elec_mun_firma,
		   coalesce((trim(to_char(f.trimestre, '99'))), 'Desconocido')					as trimestre_firma,
		   coalesce((trim(to_char(f.mes, '99'))), 'Desconocido')					as mes_firma,
		   coalesce((trim(to_char(vfp.anho, '9999'))), 'Desconocido')					as anho_publicacion,
		   coalesce((trim(to_char(vfp.anho_elec_gral, '9999'))), 'Desconocido')				as anho_elec_gral_publicacion,
		   coalesce((trim(to_char(vfp.anho_elec_mun, '9999'))), 'Desconocido')				as anho_elec_mun_publicacion,
		   coalesce((trim(to_char(vfp.trimestre, '9999'))), 'Desconocido')				as trimestre_publicacion,
		   coalesce((trim(to_char(vfp.mes, '9999'))), 'Desconocido')					as mes_publicacion,
		   case	when vppr.periodo > 365 					
				then 'Mayor a 1 anho'
			when vppr.periodo <= 365 and vppr.periodo > 335 	
				then '12 meses aprox'
			when vppr.periodo <= 335 and vppr.periodo > 305 	
				then '11 meses aprox'
			when vppr.periodo <= 305 and vppr.periodo > 270 	
				then '10 meses aprox'
			when vppr.periodo <= 270 and vppr.periodo > 240 	
				then '9 meses aprox'
			when vppr.periodo <= 240 and vppr.periodo > 210 	
				then '8 meses aprox'
			when vppr.periodo <= 210 and vppr.periodo > 180 	
				then '7 meses aprox'
			when vppr.periodo <= 180 and vppr.periodo > 150 	
				then '6 meses aprox'
			when vppr.periodo <= 150 and vppr.periodo > 120 	
				then '5 meses aprox'
			when vppr.periodo <= 120 and vppr.periodo > 90 	
				then '4 meses aprox'
			when vppr.periodo <= 90  and vppr.periodo > 60   
				then '3 meses aprox'
			when vppr.periodo <= 60  and vppr.periodo > 30   
				then '2 meses aprox'
			when vppr.periodo <= 30  and vppr.periodo > 0   
				then '1 mes aprox'
			else 'Desconocido'
		   end												as periodo_public_rpta,
		   coalesce((trim(upper(con.abuelo))), 'Desconocido')						as convocante,
		   rm.rango											as monto,
		   case when ct.id_contrato in (select m.contrato_id from origen_modificacion_2 m) 
				then '1'
		        else '0'
		        end 											as modificado
		   from contrato ct
		   left outer join v_rango_monto rm		on ct.id_contrato = rm.id
		   left outer join v_convocatoria cv 		on ct.idllamado = cv.id_llamado
		   left outer join "Origen_planificacion" op 	on ct.idllamado = op.id_llamado
		   left outer join "Vigencia_Contrato_dim" vc 	on ct.vigencia_contrato = vc.vigencia_contrato
		   --left outer join v_proveedor p 		on ct.ruc = p.ruc
		   --left outer join v_ciudad_proveedor p 	on ct.ruc = p.ruc
		   left outer join v_dpto_proveedor p 		on ct.ruc = p.ruc
		   left outer join fecha_dim f			on date(ct.fecha_firma_contrato) = f.fecha
		   left outer join v_fecha_publicacion vfp	on ct.idllamado = vfp.id_llamado
		   left outer join v_periodo_pub_ent_cie vppr	on ct.idllamado = vppr.id_llamado
		   left outer join convocante_dim con		on ct.convocante = con.convocante			
		   ;