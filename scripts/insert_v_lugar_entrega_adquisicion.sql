﻿create or replace view v_lugar_entrega_adquisicion (lugar_entrega_adquisicion)
as
SELECT DISTINCT
	trim(
		replace(
			replace(
				replace(
					replace(
						replace(
							replace(
								upper(
									trim(o.lugar_entrega_adquisicion)
								), '"', ''
							), '-', ''
						), '(', ''
					), ')', ''
				), '?', ''
			), '	', ''
		)
	)
FROM 
  public.origen_convocatoria o
order by 1;

-- " XVI REGIÓN SANITARIA  BOQUERÓN, CIUDAD DE MARISCAL ESTIGARRIBIA, SAN MIGUEL C/ RUTA TRANSCHACO"
