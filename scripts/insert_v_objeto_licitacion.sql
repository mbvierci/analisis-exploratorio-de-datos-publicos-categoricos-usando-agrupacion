﻿truncate public.v_objeto_licitacion_dim;
insert into public.v_objeto_licitacion_dim
("Objeto_licitacion", 
  "Codigo")
SELECT distinct
 op.objeto_licitacion,
 op.objeto_licitacion_codigo  
FROM 
  public."Origen_planificacion" op
  order by 1;	