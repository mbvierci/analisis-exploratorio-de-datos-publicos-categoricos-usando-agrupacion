﻿--drop sequence moneda_seq;
--CREATE SEQUENCE moneda_seq START 1;

insert into public."Moneda_dim"
( "Id",
  "Codigo", 
  "Descripcion")
SELECT
  nextval('moneda_seq') as id, 
  v."Codigo", 
  v."Moneda"
FROM 
  public.v_moneda_dim v
WHERE v."Codigo" not in (select m."Codigo"
			 from public."Moneda_dim" m);