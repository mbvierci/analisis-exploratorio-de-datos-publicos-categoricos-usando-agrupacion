﻿create or replace view v_plazo_entrega_adquisicion (plazo_entrega_adquisicion)
as
SELECT DISTINCT
	replace(
		replace(
			replace(
				replace(
					replace(
						replace(
							upper(
								trim(o.plazo_entrega_adquisicion)
							), '"', ''
						), '-', ''
					), '(', ''
				), ')', ''
			), '?', ''
		), '	', ''
	)
FROM 
  public.origen_convocatoria o
order by 1;

-- con TRIM y UPPER: bajo de 4660 a 3913
-- con el replace de '-' de 3913 a 3909
-- con el replace de '(' y ')' de 3909 a 3899
