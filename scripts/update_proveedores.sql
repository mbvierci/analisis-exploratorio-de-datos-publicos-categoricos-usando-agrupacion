﻿update datos_proveedores
set ciudad = upper(ciudad);

update datos_proveedores
set ciudad = trim(replace(ciudad, '-', ''));

update datos_proveedores
set ciudad = trim(upper(departamento))
where ciudad = '0'
and departamento <> 'None';

update datos_proveedores
set ciudad = trim(upper(pais))
where ciudad = '0'
and pais <> 'None';

update datos_proveedores
set ciudad = 'DESCONOCIDO'
where ciudad = '0'
and pais = 'None'
and departamento = 'None';

update datos_proveedores
set
	ciudad = 'ÑEMBY'
where upper(ciudad) like '%EMBY%';

update datos_proveedores
set ciudad = trim(replace(ciudad, '.', ''));

update datos_proveedores
set
	ciudad = 'ASUNCIÓN'
where upper(ciudad) like '%ACUNCION%';

update datos_proveedores
set
	ciudad = 'AREGUÁ'
where upper(ciudad) like '%AREG%';

update datos_proveedores
set
	ciudad = 'ASUNCIÓN'
where upper(ciudad) like '%ASSU%';

update datos_proveedores
set
	ciudad = 'ASUNCIÓN'
where upper(ciudad) like '%ASUN%'
and departamento = 'Central';

update datos_proveedores
set
	ciudad = 'ASUNCIÓN'
where departamento = 'Asunción';

update datos_proveedores
set
	ciudad = 'ASUNCIÓN'
where upper(ciudad) like '%ASUN%'
and departamento = 'None'
and pais = 'Paraguay';

update datos_proveedores
set
	ciudad = ciudad || '-' || pais
where upper(ciudad) like '%ASUN%'
and departamento = 'None'
and pais <> 'Paraguay';

update datos_proveedores
set
	ciudad = 'Asunción' || '-' || departamento
where upper(ciudad) like '%ASUN%'
and departamento <> 'None'
and pais = 'Paraguay';

update datos_proveedores
set
	ciudad = 'ASUNCIÓN'
where upper(ciudad) like '%ASUN%'
and departamento = 'None'
and pais = 'None';

update datos_proveedores
set
	ciudad = 'ASUNCIÓN'
where upper(ciudad) like '%AUSNCION%';

update datos_proveedores
set ciudad = trim(replace(ciudad, '(MUNICIPIO)', ''));

update datos_proveedores
set
	ciudad = ciudad || '-' || departamento
where upper(ciudad) like '%AYOLAS%'
and departamento <> 'None';

update datos_proveedores
set
	ciudad = 'BELLA VISTA'
where upper(ciudad) like '%BELLA VISTA%';

update datos_proveedores
set
	ciudad = 'BENJAMIN ACEVAL'
where upper(ciudad) like '%BENJAMIN ACEVAL%';

update datos_proveedores
set
	ciudad = 'CAACUPÉ'
where upper(ciudad) like '%CAAC%';

update datos_proveedores
set
	ciudad = 'CAAGUAZÚ'
where upper(ciudad) like '%CAAG%';

update datos_proveedores
set
	ciudad = 'CAAZAPÁ'
where upper(ciudad) like '%CAAZ%';

update datos_proveedores
set
	ciudad = 'CAPIATÁ'
where upper(ciudad) like '%CAPIA%';

update datos_proveedores
set
	ciudad = 'CAPIIBARY'
where upper(ciudad) like '%CAPII%';

update datos_proveedores
set
	ciudad = 'CAPITÁN MIRANDA'
where upper(ciudad) like '%CAPITAN MIRANDA%';

update datos_proveedores
set
	ciudad = 'CARAPEGUÁ'
where upper(ciudad) like '%CARAP%';

update datos_proveedores
set
	ciudad = 'CARLOS ANTONIO LÓPEZ'
where upper(ciudad) like '%CARLOS%';

update datos_proveedores
set
	ciudad = 'CIUDAD DEL ESTE'
where upper(ciudad) like 'CDE';

update datos_proveedores
set
	ciudad = 'CHORÉ'
where upper(ciudad) like '%CHOR%';

update datos_proveedores
set
	ciudad = 'BUENOS AIRES'
where upper(ciudad) like '%BUENOS AIRES%';

update datos_proveedores
set
	ciudad = 'CIUDAD DEL ESTE'
where upper(ciudad) like '%CIUDAD DEL ESTE%';

update datos_proveedores
set
	ciudad = 'CIUDAD DEL ESTE'
where upper(ciudad) like '%CIUDAD EL ESTE%';

update datos_proveedores
set
	ciudad = 'CORONEL OVIEDO'
where upper(ciudad) like '%OVIEDO%'
and upper(ciudad) not like '%RAUL%';

update datos_proveedores
set
	ciudad = 'COLONIA IRUÑA'
where upper(ciudad) like '%COLONIA IRU%';

update datos_proveedores
set
	ciudad = 'CONCEPCIÓN'
where upper(ciudad) like 'CONC%';

update datos_proveedores
set
	ciudad = 'CORONEL MARTINEZ'
where upper(ciudad) like '%CNEL MARTINEZ%';

update datos_proveedores
set
	ciudad = 'CORRIENTES'
where upper(ciudad) like '%CORRIENTE%';

update datos_proveedores
set
	ciudad = 'CIUDAD DEL ESTE'
where upper(ciudad) like '%CUIDAD DEL ESTE%';

update datos_proveedores
set
	ciudad = 'CURITIBA'
where upper(ciudad) like '%CURITIBA%';

update datos_proveedores
set
	ciudad = 'CIUDAD DEL ESTE'
where upper(ciudad) like '%DEL ESTE%';

update datos_proveedores
set
	ciudad = 'MBARACAYÚ'
where upper(ciudad) like '%MBARACAY%';

update datos_proveedores
set
	ciudad = 'DOMINGO MARTINEZ DE IRALA'
where upper(ciudad) like '%DOMINGO M%';

update datos_proveedores
set
	ciudad = 'EUSEBIO AYALA'
where upper(ciudad) like '%AYALA%';

update datos_proveedores
set
	ciudad = 'EDELIRA'
where upper(ciudad) like '%EDEL%';

update datos_proveedores
set
	ciudad = 'ENCARNACIÓN'
where upper(ciudad) like '%ENCAR%';

update datos_proveedores
set ciudad = replace(ciudad, '  ', ' ');

update datos_proveedores
set
	ciudad = 'FERNANDO DE LA MORA'
where upper(ciudad) like '%DE LA MORA%';

update datos_proveedores
set
	ciudad = 'FERNANDO DE LA MORA'
where upper(ciudad) like '%FERNANDO DE MORA%';

update datos_proveedores
set ciudad = trim(replace(ciudad, '( MUNICIPIO )', ''));

update datos_proveedores
set
	ciudad = 'FRANCISCO CABALLERO ÁLVAREZ'
where upper(ciudad) like '%FRANCISCO CABALLERO%';

update datos_proveedores
set
	ciudad = 'GENERAL ARTIGAS'
where upper(ciudad) like '%GRAL AR%';

update datos_proveedores
set
	ciudad = 'GENERAL DELGADO'
where upper(ciudad) like '%GRAL DELGADO%';

update datos_proveedores
set
	ciudad = 'GUAYAIBÍ'
where upper(ciudad) like '%GUAJ%'
or upper(ciudad) like '%GUAYA%';

update datos_proveedores
set
	ciudad = 'HERNANDARIAS'
where upper(ciudad) like '%HERNA%';

update datos_proveedores
set
	ciudad = 'HOHENAU'
where upper(ciudad) like 'HOHENAU%';

update datos_proveedores
set
	ciudad = 'ITÁ'
where upper(ciudad) like 'ITA'
or upper(ciudad) like 'ITA %';

update datos_proveedores
set
	ciudad = 'ITACURUBI DEL ROSARIO'
where upper(ciudad) like 'ITACDEL ROSARIO';

update datos_proveedores
set
	ciudad = 'ITACURUBÍ DE LA CORDILLERA'
where upper(ciudad) like 'ITACURUBI DE LA CORDILLERA';

update datos_proveedores
set
	ciudad = 'ITAUGUÁ'
where upper(ciudad) like 'ITAG%'
or upper(ciudad) like 'ITAU%';

update datos_proveedores
set
	ciudad = 'ITAPÚA'
where upper(ciudad) like 'ITAPÃ?A';

update datos_proveedores
set
	ciudad = 'J AUGUSTO SALDÍVAR'
where upper(ciudad) like '%AUGUSTO SALDIVAR%';

update datos_proveedores
set
	ciudad = 'J EULOGIO ESTIGARRIBIA'
where upper(ciudad) like '%J E ESTIGARRIBIA%';

update datos_proveedores
set
	ciudad = 'J AUGUSTO SALDÍVAR'
where upper(ciudad) like '%JA SALDIVAR%';

update datos_proveedores
set
	ciudad = 'J AUGUSTO SALDÍVAR'
where upper(ciudad) like '%JASALDIVAR%';

update datos_proveedores
set
	ciudad = 'J EULOGIO ESTIGARRIBIA'
where upper(ciudad) like '%JE ESTIGARRIBIA%';

update datos_proveedores
set
	ciudad = 'J EULOGIO ESTIGARRIBIA'
where upper(ciudad) like '%JEULOGIO ESTIGARRIBIA%';

update datos_proveedores
set
	ciudad = 'JOSÉ DOMINGO OCAMPOS'
where upper(ciudad) like '%OCAMPOS%';

update datos_proveedores
set
	ciudad = 'JUAN E O LEARY'
where upper(ciudad) like '%LEARY%';

update datos_proveedores
set
	ciudad = 'JUAN LEÓN MALLORQUÍN'
where upper(ciudad) like '%MALLORQUIN%';

update datos_proveedores
set
	ciudad = 'JULIO D OTAÑO'
where upper(ciudad) like '%OTAÑO%';

update datos_proveedores
set
	ciudad = 'LA PALOMA'
where upper(ciudad) like '%LA PALOMA%';

update datos_proveedores
set
	ciudad = 'LAMBARÉ'
where upper(ciudad) like '%LAM%';

update datos_proveedores
set
	ciudad = 'LIMPIO'
where upper(ciudad) like '%LUMPIO%';

update datos_proveedores
set
	ciudad = 'MARÍA AUXILIADORA'
where upper(ciudad) like '%MARIA AUXILIADORA%';

update datos_proveedores
set
	ciudad = 'MARIANO ROQUE ALONSO'
where upper(ciudad) like '%MARIANO%';

update datos_proveedores
set
	ciudad = 'MAYOR MARTINEZ'
where upper(ciudad) like '%MAYOR JDMARTINEZ%';

update datos_proveedores
set
	ciudad = 'MARISCAL ESTIGARRIBIA'
where upper(ciudad) like '%MCAL%'
and upper(ciudad) like '%ESTIGARRIBIA%';

update datos_proveedores
set
	ciudad = 'MINGA GUAZÚ'
where upper(ciudad) like '%MINGA G%';

update datos_proveedores
set
	ciudad = 'MONTEVIDEO'
where upper(ciudad) like '%MONTEVIDEO%';

update datos_proveedores
set
	ciudad = 'MARIANO ROQUE ALONSO'
where upper(ciudad) like '%MRALONSO%';

update datos_proveedores
set
	ciudad = 'MUMBAI'
where upper(ciudad) like '%MUMBAI%';

update datos_proveedores
set
	ciudad = 'NUEVA ESPERANZA'
where upper(ciudad) like '%NUEVA ESPERANZA%';

update datos_proveedores
set
	ciudad = 'DESCONOCIDO'
where upper(ciudad) like '%NULL%';

update datos_proveedores
set
	ciudad = 'PARAGUARÍ'
where upper(ciudad) like '%PARAGAUYRI%'
or upper(ciudad) like '%PARAGUARI%';

update datos_proveedores
set
	ciudad = 'PEDRO JUAN CABALLERO'
where upper(ciudad) like '%JUAN CABALLERO%'
or upper(ciudad) like '%PEDRO JUANCABALLERO%';

update datos_proveedores
set
	ciudad = 'PRESIDENTE FRANCO'
where upper(ciudad) like '%PDTEFRANCO%'
or upper(ciudad) like '%PDTE FRANCO%';

update datos_proveedores
set
	ciudad = departamento
where upper(ciudad) like '%PENDIENTE ACTUALIZACION DATOS%'
and departamento <> 'None';

update datos_proveedores
set
	ciudad = pais
where upper(ciudad) like '%PENDIENTE ACTUALIZACION DATOS%'
and departamento = 'None'
and pais <> 'None';

update datos_proveedores
set
	ciudad = 'Desconocido'
where upper(ciudad) like '%PENDIENTE ACTUALIZACION DATOS%'
and departamento = 'None'
and pais = 'None';

update datos_proveedores
set
	ciudad = 'PILAR'
where upper(ciudad) like '%PILAE%';

update datos_proveedores
set
	ciudad = 'PRESIDENTE FRANCO'
where upper(ciudad) like '%PTE FRANCO%'
or upper(ciudad) like '%PTEFRANCO%';

update datos_proveedores
set
	ciudad = 'RAUL ARSENIO OVIEDO'
where upper(ciudad) like '%RAUL A OVIEDO%';

update datos_proveedores
set
	ciudad = 'SALTO DEL GUAIRÁ'
where upper(ciudad) like '%SALTO%';

update datos_proveedores
set
	ciudad = 'SAN COSME Y DAMIÁN'
where upper(ciudad) like '%SAN COSME Y DAMIAN%';

update datos_proveedores
set
	ciudad = 'SAN IGNACIO'
where upper(ciudad) like '%SAN IGNACIO%';

update datos_proveedores
set
	ciudad = ciudad || '-' || pais
where upper(ciudad) like '%SAN ISIDRO%'
and pais = 'Argentina';

update datos_proveedores
set
	ciudad = 'SAN JUAN NEPOMUCENO'
where upper(ciudad) like '%NEPOMUCENO%';

update datos_proveedores
set
	ciudad = 'SAN JUAN BAUTISTA'
where upper(ciudad) like '%BAUTISTA%';

update datos_proveedores
set
	ciudad = 'SAN LÁZARO'
where upper(ciudad) like '%LAZARO%';

update datos_proveedores
set
	ciudad = 'SAN LORENZO'
where upper(ciudad) like '%LORENZO%'
or upper(ciudad) like '%LORENSO%';

update datos_proveedores
set
	ciudad = 'SAN MIGUEL'
where upper(ciudad) like '%SAN MIGUEL%';

update datos_proveedores
set
	ciudad = 'SAN PEDRO DE YCUAMANDYYU'
where upper(ciudad) like '%SAN PEDRO%'
and departamento = 'San Pedro';

update datos_proveedores
set
	ciudad = 'SAN PEDRO DEL PARANÁ'
where upper(ciudad) like '%SAN PEDRO%'
and departamento = 'Itapúa';

update datos_proveedores
set
	ciudad = 'SAN RAFAEL DEL PARANÁ'
where upper(ciudad) like '%SAN RAFAEL DEL PARANA%';

update datos_proveedores
set
	ciudad = 'SAN ROQUE GONZÁLEZ DE SANTA CRUZ'
where upper(ciudad) like '%SAN ROQUE GONZALEZ%';

update datos_proveedores
set
	ciudad = 'SANTA ROSA DEL MONDAY'
where upper(ciudad) like '%SANTA ROSA DEL MODAY%';

update datos_proveedores
set
	ciudad = 'SANTIAGO MISIONES'
where upper(ciudad) like '%SANTIAGO%'
and departamento = 'Misiones';

update datos_proveedores
set
	ciudad = 'TOBATÍ'
where upper(ciudad) like '%TOBAT%';

update datos_proveedores
set
	ciudad = 'TOMÁS ROMERO PEREIRA'
where upper(ciudad) like '%TOMAS ROMERO PEREIRA%';

update datos_proveedores
set
	ciudad = 'TTE MANUEL IRALA FERNANDEZ'
where upper(ciudad) like '%MANUEL IRALA FERNANDEZ%';

update datos_proveedores
set
	ciudad = 'VALLEMÍ'
where upper(ciudad) like '%VALLE%';

update datos_proveedores
set
	ciudad = 'VILLA ELISA'
where upper(ciudad) like '%VILLA ELISA%'
or upper(ciudad) like '%VILLAELISA%';

update datos_proveedores
set
	ciudad = 'VILLARRICA'
where upper(ciudad) like '%VILLARICA%'
or upper(ciudad) like '%VILLARRICA%';

update datos_proveedores
set
	ciudad = 'YAGUARÓN'
where upper(ciudad) like '%YAGUARON%';

update datos_proveedores
set
	ciudad = 'YBY YAÚ'
where upper(ciudad) like 'YBY %';

update datos_proveedores
set
	ciudad = 'YPACARAÍ'
where upper(ciudad) like 'YPACARA%';

update datos_proveedores
set
	ciudad = 'YPANÉ'
where upper(ciudad) like 'YPANE%';

update datos_proveedores
set
	ciudad = 'YUTY'
where upper(ciudad) like 'YUTY%';

update datos_proveedores
set
	ciudad = initcap(ciudad);