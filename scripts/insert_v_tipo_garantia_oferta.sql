﻿truncate public.v_tipo_garantia_oferta;

insert into public.v_tipo_garantia_oferta
(tipo_garantia_oferta, 
  codigo)
SELECT distinct
 op.tipo_garantia_oferta,
 op.tipo_garantia_oferta_codigo  
FROM 
  public.origen_convocatoria op
  order by 1;