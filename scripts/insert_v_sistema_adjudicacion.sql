﻿truncate public.v_sistema_adjudicacion_dim;
insert into public.v_sistema_adjudicacion_dim
(sistema_adjudicacion, 
  codigo)
SELECT distinct
 op.sistema_adjudicacion,
 op.sistema_adjudicacion_codigo
FROM 
  public.origen_convocatoria op
  order by 1;