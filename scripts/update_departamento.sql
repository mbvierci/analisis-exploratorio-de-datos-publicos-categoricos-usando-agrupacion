﻿
/**/

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where upper(p.ciudad) = upper(t.ciudad);

update datos_proveedores
set departamento = pais
where pais not in ('Paraguay', 'None');

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where p.ciudad = '1ro De Marzo'
and t.ciudad = 'Primero de Marzo';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where p.ciudad like 'Aba%'
and t.ciudad like 'Aba%';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where UPPER(p.ciudad) like '%ACEVAL%'
and UPPER(t.ciudad) like '%ACEVAL%';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where UPPER(p.ciudad) like '%BELEN%'
and UPPER(t.ciudad) like '%BELÉN%';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where UPPER(p.ciudad) like '%CAMBY%'
and UPPER(t.ciudad) like '%CAMBY%';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where UPPER(p.ciudad) like '%MEZA%'
and UPPER(t.ciudad) like '%MEZA%';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where UPPER(p.ciudad) like '%CARAY%'
and UPPER(t.ciudad) like '%CARAY%';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where UPPER(p.ciudad) like '%CARMEN%'
and UPPER(t.ciudad) like '%CARMEN%';

update datos_proveedores p
set departamento = t.departamento,
	ciudad = 'Coronel José Félix Bogado'
from ciudad_dpto t
where p.ciudad like '%Cneljose Fbogado%'
and UPPER(t.ciudad) like '%BOGADO%';

update datos_proveedores p
set departamento = t.departamento,
	ciudad = 'Doctor Juan Manuel Frutos'
from ciudad_dpto t
where p.ciudad like '%Coljuan M Frutos%'
and UPPER(t.ciudad) like '%FRUTOS%';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where UPPER(p.ciudad) like '%IRUÑA%'
and UPPER(t.ciudad) like '%IRUÑA%';

update datos_proveedores p
set departamento = t.departamento
from datos_proveedores t
where p.departamento = 'None'
and t.departamento <> 'None'
and t.ciudad = p.ciudad;

update datos_proveedores p
set departamento = t.departamento,
	ciudad = 'General Higinio Morínigo'
from ciudad_dpto t
where p.ciudad like '%Graalh Morinigo (Munic)%'
and UPPER(t.ciudad) like '%MOR%';

update datos_proveedores p
set departamento = t.departamento,
	ciudad = 'General Elizardo Aquino'
from ciudad_dpto t
where p.ciudad like '%Gral Eaquino%'
and UPPER(t.ciudad) like '%AQUINO%';

update datos_proveedores p
set departamento = t.departamento,
	ciudad = 'General Artigas'
from ciudad_dpto t
where p.ciudad like '%Gralgervacio Artigas (Munic)%'
and UPPER(t.ciudad) like '%ARTIGAS%';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where UPPER(p.ciudad) like '%HUMAIT%'
and UPPER(t.ciudad) like '%HUMAIT%';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where UPPER(p.ciudad) like '%ITAPE%'
and UPPER(t.ciudad) like '%ITAPÉ%';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where UPPER(p.ciudad) like '%JESUS%'
and UPPER(t.ciudad) like '%JESÚS%';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where UPPER(p.ciudad) like '%FALC%'
and UPPER(t.ciudad) like '%FALC%';

update datos_proveedores p
set departamento = t.departamento,
	ciudad = 'Mariscal Francisco Solano Lópezs'
from ciudad_dpto t
where p.ciudad like '%Mcalfsolano Lopez%'
and UPPER(t.ciudad) like '%SOLANO%';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where UPPER(p.ciudad) like '%NATALICIO%'
and UPPER(t.ciudad) like '%NATALICIO%';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where UPPER(p.ciudad) like '%PASO YOB%'
and UPPER(t.ciudad) like '%PASO YOB%';

update datos_proveedores p
set departamento = t.departamento,
	ciudad = 'R. I. Tres Corrales'
from ciudad_dpto t
where p.ciudad like '%Ri3 Corrales%'
and UPPER(t.ciudad) like '%CORRALES%';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where UPPER(p.ciudad) like '%SAN CRISTOBAL%'
and UPPER(t.ciudad) like '%SAN CRISTÓBAL%';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where UPPER(p.ciudad) like '%SAN PEDRO DEL PARANA%'
and UPPER(t.ciudad) like '%SAN PEDRO DEL PARANÁ%';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where UPPER(p.ciudad) like '%YGATIMI%'
and UPPER(t.ciudad) like '%YGATIMÍ%';

update datos_proveedores p
set departamento = t.departamento
from ciudad_dpto t
where UPPER(p.ciudad) like '%YRYBUCUA%'
and UPPER(t.ciudad) like '%YRYBUCUÁ%';

update datos_proveedores p
set departamento = 'Desconocido'
where p.departamento = 'None';

update datos_proveedores p
set departamento = 'Capital'
where p.departamento = 'Asunción';

update datos_proveedores p
set departamento = initcap(departamento);

/**/


