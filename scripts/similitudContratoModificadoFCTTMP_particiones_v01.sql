﻿--TMP DE SIMILITUD DE CONTRATO 1
drop table similitud_contrato_modificado_fct_tmp_1;
CREATE TABLE similitud_contrato_modificado_fct_tmp_1
(
  id_contrato				character varying(500),
  tipo_garantia_oferta 			character varying(500),
  sistema_adjudicacion 			character varying(500),
  objeto_licitacion 			character varying(500),
  forma_pago 				character varying(500),
  categoria 				character varying(500),
  tipo_procedimiento 			character varying(500),
  vigencia_contrato			character varying(500),
  dpto_proveedor 			character varying(500),
  anho_firma				character varying(500),
  anho_elec_gral_firma			character varying(500),
  anho_elec_mun_firma			character varying(500),
  trimestre_firma			character varying(500),
  mes_firma				character varying(500),
  anho_publicacion			character varying(500),
  anho_elec_gral_publicacion		character varying(500),
  anho_elec_mun_publicacion		character varying(500),
  trimestre_publicacion			character varying(500),
  mes_publicacion			character varying(500),
  periodo_public_rpta			character varying(500),
  convocante				character varying(500),
  monto					bigint,
  modificado 				character varying(500)
)
WITH (
  OIDS=FALSE
);

--CARGA DE SIMILITUD DE CONTRATO 1
truncate table similitud_contrato_modificado_fct_tmp_1;
INSERT INTO similitud_contrato_modificado_fct_tmp_1
(select * from similitud_contrato_modificado_fct_tmp where random() < 0.4);

--40167   

--TMP DE SIMILITUD DE CONTRATO 2
drop table similitud_contrato_modificado_fct_tmp_2;
CREATE TABLE similitud_contrato_modificado_fct_tmp_2
(
  id_contrato				character varying(500),
  tipo_garantia_oferta 			character varying(500),
  sistema_adjudicacion 			character varying(500),
  objeto_licitacion 			character varying(500),
  forma_pago 				character varying(500),
  categoria 				character varying(500),
  tipo_procedimiento 			character varying(500),
  vigencia_contrato			character varying(500),
  dpto_proveedor 			character varying(500),
  anho_firma				character varying(500),
  anho_elec_gral_firma			character varying(500),
  anho_elec_mun_firma			character varying(500),
  trimestre_firma			character varying(500),
  mes_firma				character varying(500),
  anho_publicacion			character varying(500),
  anho_elec_gral_publicacion		character varying(500),
  anho_elec_mun_publicacion		character varying(500),
  trimestre_publicacion			character varying(500),
  mes_publicacion			character varying(500),
  periodo_public_rpta			character varying(500),
  convocante				character varying(500),
  monto					bigint,
  modificado 				character varying(500)
)
WITH (
  OIDS=FALSE
);

--CARGA DE SIMILITUD DE CONTRATO 2
truncate table similitud_contrato_modificado_fct_tmp_2;
INSERT INTO similitud_contrato_modificado_fct_tmp_2
(select * from similitud_contrato_modificado_fct_tmp t
 where random() < 0.4
 and t.id_contrato not in (select id_contrato from similitud_contrato_modificado_fct_tmp_1));

 --24046   

 --TMP DE SIMILITUD DE CONTRATO 3
drop table similitud_contrato_modificado_fct_tmp_3;
CREATE TABLE similitud_contrato_modificado_fct_tmp_3
(
  id_contrato				character varying(500),
  tipo_garantia_oferta 			character varying(500),
  sistema_adjudicacion 			character varying(500),
  objeto_licitacion 			character varying(500),
  forma_pago 				character varying(500),
  categoria 				character varying(500),
  tipo_procedimiento 			character varying(500),
  vigencia_contrato			character varying(500),
  dpto_proveedor 			character varying(500),
  anho_firma				character varying(500),
  anho_elec_gral_firma			character varying(500),
  anho_elec_mun_firma			character varying(500),
  trimestre_firma			character varying(500),
  mes_firma				character varying(500),
  anho_publicacion			character varying(500),
  anho_elec_gral_publicacion		character varying(500),
  anho_elec_mun_publicacion		character varying(500),
  trimestre_publicacion			character varying(500),
  mes_publicacion			character varying(500),
  periodo_public_rpta			character varying(500),
  convocante				character varying(500),
  monto					bigint,
  modificado 				character varying(500)
)
WITH (
  OIDS=FALSE
);

--CARGA DE SIMILITUD DE CONTRATO 3
truncate table similitud_contrato_modificado_fct_tmp_3;
INSERT INTO similitud_contrato_modificado_fct_tmp_3
(select * from similitud_contrato_modificado_fct_tmp t
 where t.id_contrato not in (select id_contrato from similitud_contrato_modificado_fct_tmp_1)
 and t.id_contrato not in (select id_contrato from similitud_contrato_modificado_fct_tmp_2));

 --35969   