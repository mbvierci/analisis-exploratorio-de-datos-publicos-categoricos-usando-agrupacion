﻿truncate public.v_estado_dim;
insert into public.v_estado_dim
("Estado", 
  "Codigo")
SELECT distinct
 op.estado,
 op.estado_codigo  
FROM 
  public.origen_convocatoria op
  order by 1;