﻿CREATE OR REPLACE VIEW v_dpto_proveedor AS 
 SELECT DISTINCT
	dp.ruc,
	dp.departamento
 FROM datos_proveedores dp
 ORDER BY dp.ruc asc;