﻿--truncate table public."Tipo_Procedimiento_dim";
insert into public."Tipo_Procedimiento_dim"(
	"Id",
	"Tipo_Procedimiento",
	"Codigo")
SELECT distinct
  min(o.tipo_procedimiento_id), 
  trim(o.tipo_procedimiento),
  max(o.tipo_procedimiento_codigo)
FROM 
  public.origen_convocatoria o
group by trim(o.tipo_procedimiento)
having trim(o.tipo_procedimiento) not in (select t."Tipo_Procedimiento"
				    from public."Tipo_Procedimiento_dim" t)
and min(o.tipo_procedimiento_id) not in (select t."Id"
				    from public."Tipo_Procedimiento_dim" t)
order by 1;