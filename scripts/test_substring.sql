﻿SELECT 
  t.id_contrato,
  substring(t.id_contrato, 1, 6),
  substring(t.id_contrato, -1, 2)
FROM 
  public.similitud_contrato_modificado_fct_tmp t
  order by 1;

SELECT SUBSTRING('XY1234Z', 'Y*([0-9]{1,3})');
Result: 123