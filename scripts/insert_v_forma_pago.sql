﻿truncate public.v_forma_pago_dim;

insert into public.v_forma_pago_dim
(forma_pago, 
  codigo)
SELECT distinct
 op.forma_pago,
 op.forma_pago_codigo
FROM 
  public.origen_convocatoria op
  order by 1;