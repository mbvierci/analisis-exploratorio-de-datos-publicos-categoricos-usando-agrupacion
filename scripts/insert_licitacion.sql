﻿--truncate table public."Licitacion_dim";
insert into public."Licitacion_dim"(
	"Licitacion",
	"Id")
select distinct
	ltrim(
		trim(
			ltrim(
				replace(
					replace(
						replace(
							replace(
								initcap(
									trim(o.nombre_licitacion)
								), '"', ''
							), '.', ''
						), '-', ''
					), '?', ''
				), ',¡'
			)
		), '	'
	),
	o.id_llamado
from public.origen_convocatoria o
where o.id_llamado not in (select l."Id"
			   from public."Licitacion_dim" l)
order by 1;