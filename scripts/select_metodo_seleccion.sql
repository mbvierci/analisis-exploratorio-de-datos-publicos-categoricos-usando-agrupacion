﻿SELECT  distinct (
		case when (trim (replace( upper( trim(o.metodo_seleccion) ), '"', '') ) ) like '%158%'
			then 'Seleccion basada en calidad y costo'
		     when (trim (replace( upper( trim(o.metodo_seleccion) ), '"', '') ) ) like '%159%'
			then 'Seleccion basada en calidad'
		     when (trim (replace( upper( trim(o.metodo_seleccion) ), '"', '') ) ) like '%160%'
			then 'Seleccion basada en presupuesto fijo'
		     when (trim (replace( upper( trim(o.metodo_seleccion) ), '"', '') ) ) like '%161%'
			then 'Seleccion basada en precio'
		     when (trim (replace( upper( trim(o.metodo_seleccion) ), '"', '') ) ) like '%162%'
			then 'Seleccion basada en antecedentes'
		     when (trim (replace( upper( trim(o.metodo_seleccion) ), '"', '') ) ) = '-2'
			then 'Desconocido'
		     when (trim (replace( upper( trim(o.metodo_seleccion) ), '"', '') ) ) = ''
			then 'Desconocido'		
	             else (trim (replace( upper( trim(o.metodo_seleccion) ), '"', '') ) )
	        end 
  ) as metodo_seleccion
FROM 
  public.origen_convocatoria o;

-- 158 seleccion basada en calidad y costo
-- 159 seleccion basada en calidad
-- 160 seleccion basada en presupuesto fijo
-- 161 seleccion basada en precio
-- 162 seleccion basada en antecedentes
-- -2
-- null ""