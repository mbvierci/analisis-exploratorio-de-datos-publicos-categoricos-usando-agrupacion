﻿--drop sequence estado_seq;
CREATE SEQUENCE tipo_garantia_oferta_seq START 1;

insert into public.tipo_garantia_oferta
( id,
  tipo_garantia_oferta_codigo, 
  tipo_garantia_oferta)
SELECT
  nextval('tipo_garantia_oferta_seq') as id, 
  v.codigo, 
  v.tipo_garantia_oferta
FROM 
  public.v_tipo_garantia_oferta v
where v.codigo not in (select t.tipo_garantia_oferta_codigo
			 from public.tipo_garantia_oferta t);