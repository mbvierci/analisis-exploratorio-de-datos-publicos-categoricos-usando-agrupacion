﻿create or replace view v_restriccion (restriccion)
as
SELECT DISTINCT
	upper(
		trim(
			replace(
				replace(
					replace(
						replace(o.restricciones, '-', ''), '"', ''
					), '_', ''
				), '.', ''
			)
		)
	)
FROM 
  public.origen_convocatoria o
order by 1;

-- con trim de 224 a 221
-- con replace - va a 218
-- con replace " va a 217
-- con replace _ va a 214
-- con upper va a 204
-- con replace . va a 188