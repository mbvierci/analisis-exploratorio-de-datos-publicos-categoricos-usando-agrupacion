﻿/**********CATEGORIA**********/
insert into public."Categoria_dim"
("Id", 
  "Codigo", 
  "Categoria")
SELECT distinct
  "Origen_planificacion".categoria_id, 
  "Origen_planificacion".categoria_codigo, 
  "Origen_planificacion".categoria
FROM 
  public."Origen_planificacion"
where "Origen_planificacion".categoria_id not in (select "Categoria_dim"."Id" 
						  from public."Categoria_dim")
order by 1 asc;

/**********CONVOCANTE**********/
--SI EL CONVOCANTE YA EXISTE EN LA DIMENSION Y EL NUEVO TIENE PADRE Y/O ABUELO
UPDATE public."Convocante_dim" c SET
	"Padre" = 	v."Padre",
	"Abuelo" = 	v."Abuelo"
from public.v_convocante_dim v
where v."Convocante" = c."Convocante"
and v."Padre" is not null
and v."Abuelo" is not null;

--SI EL CONVOCANTE NO EXISTE EN LA DIMENSION
insert into public."Convocante_dim"
( "Id", 
  "Convocante", 
  "Padre", 
  "Abuelo")
select
  nextval('convocante_seq') as id,
  v."Convocante" as convocante,
  case  when v."Padre" is not null then v."Padre"
	when v."Padre" is null then
		case when v."Convocante" in (select distinct vc."Padre"
					  from public.v_convocante_dim vc
					  where vc."Padre" is not null) then (select max(vc."Padre")
									      from public.v_convocante_dim vc
									      where vc."Padre" = v."Convocante")
		when v."Convocante" in (select distinct vc."Abuelo"
				      from public.v_convocante_dim vc
				      where vc."Abuelo" is not null) then (select vc."Abuelo"
									   from public.v_convocante_dim vc
									   where vc."Abuelo" = v."Convocante")
		else 'Desconocido'
		end
	else 'Desconocido'
  end as padre,
  case when v."Abuelo" is not null then v."Abuelo"
       when v."Abuelo" is null then
		case when v."Convocante" in (select distinct vc."Padre"
					  from public.v_convocante_dim vc
					  where vc."Padre" is not null) then (select max(vc."Abuelo")
									      from public.v_convocante_dim vc
									      where vc."Padre" = v."Convocante")
		when v."Convocante" in (select distinct vc."Abuelo"
				      from public.v_convocante_dim vc
				      where vc."Abuelo" is not null) then (select vc."Abuelo"
									   from public.v_convocante_dim vc
									   where vc."Abuelo" = v."Convocante")
		else 'Desconocido'
		end
	else 'Desconocido'
  end as abuelo
from public.v_convocante_dim v
where v."Convocante" not in (select "Convocante_dim"."Convocante"
			     from public."Convocante_dim");

/**********ESTADO**********/
insert into public."Estado_dim"
( "Id",
  "Codigo", 
  "Estado")
SELECT
  nextval('estado_seq') as id, 
  v."Codigo", 
  v."Estado"
FROM 
  public.v_estado_dim v
where v."Codigo" not in (select e."Codigo"
			 from public."Estado_dim" e);

/**********FECHA**********/
insert into public."Fecha_dim"
("Fecha", 
 "Dia", 
 "Mes", 
 "Mes_descripcion", 
 "Año")
select 
	distinct "Origen_planificacion".fecha_publicacion as fecha,
	date_part('day', "Origen_planificacion".fecha_publicacion) as dia,
	date_part('month', "Origen_planificacion".fecha_publicacion) as mes,
	case date_part('month', "Origen_planificacion".fecha_publicacion)
		when 1 then 'Enero'
		when 2 then 'Febrero'
		when 3 then 'Marzo'
		when 4 then 'Abril'
		when 5 then 'Mayo'
		when 6 then 'Junio'
		when 7 then 'Julio'
		when 8 then 'Agosto'
		when 9 then 'Setiembre'
		when 10 then 'Octubre'
		when 11 then 'Noviembre'
		when 12 then 'Diciembre'
		else 'Desconocido'
	end as mes_descripcion,
	date_part('year', "Origen_planificacion".fecha_publicacion) as anho
from public."Origen_planificacion"
where "Origen_planificacion".fecha_publicacion not in (select distinct "Fecha_dim"."Fecha"
							from public."Fecha_dim")
order by 1;

/**********LICITACION**********/
insert into public."Licitacion_dim"(
	"Licitacion",
	"Id")
select distinct
	trim(
		ltrim(
			replace(
				replace(
					replace(
						replace(
							initcap(
								trim(o.nombre_licitacion)
							), '"', ''
						), '.', ''
					), '-', ''
				), '?', ''
			), ',¡'
		)
	),
	o.id_llamado
from public."Origen_planificacion" o
where o.id_llamado not in (select l."Id"
			   from public."Licitacion_dim" l)
order by 1;

/*********MONEDA*********/
insert into public."Moneda_dim"
( "Id",
  "Codigo", 
  "Descripcion")
SELECT
  nextval('moneda_seq') as id, 
  v."Codigo", 
  v."Moneda"
FROM 
  public.v_moneda_dim v
WHERE v."Codigo" not in (select m."Codigo"
			 from public."Moneda_dim" m);

/**********OBJETO LICITACION**********/
insert into public."Objeto_licitacion_dim"
( "Id",
  "Codigo", 
  "Objeto_licitacion")
SELECT
  nextval('objeto_licitacion_seq') as id, 
  v."Codigo", 
  v."Objeto_licitacion"
FROM 
  public.v_objeto_licitacion_dim v
WHERE v."Codigo" not in (select o."Codigo"
			 from public."Objeto_licitacion_dim" o);

/**********TIPO PROCEDIMIENTOS**********/
insert into public."Tipo_Procedimiento_dim"(
	"Id",
	"Tipo_Procedimiento",
	"Codigo")
SELECT distinct
  min(o.tipo_procedimiento_id), 
  trim(o.tipo_procedimiento),
  max(o.tipo_procedimiento_codigo)
FROM 
  public."Origen_planificacion" o
group by trim(o.tipo_procedimiento)
having trim(o.tipo_procedimiento) not in (select t."Tipo_Procedimiento"
				    from public."Tipo_Procedimiento_dim" t)
order by 1;