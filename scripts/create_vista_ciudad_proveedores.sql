﻿CREATE OR REPLACE VIEW v_ciudad_proveedor AS 
 SELECT DISTINCT
	dp.ruc,
	dp.ciudad
 FROM datos_proveedores dp
 ORDER BY dp.ruc asc;