﻿update public.convocante_dim
set
	padre  = 'Administraciones',
	abuelo = 'Administraciones'
where hijo like '%Administración%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Agencias',
	abuelo = 'Agencias'
where hijo like '%Agencia%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Agencias',
	abuelo = 'Agencias'
where hijo = 'Ag. Nac. de Eval. y Acreditación de la Educación Superior (ANEAES)'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Bancos',
	abuelo = 'Bancos'
where hijo like '%Banco%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Cajas',
	abuelo = 'Cajas'
where hijo like '%Caja%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Cajas',
	abuelo = 'Cajas'
where hijo = 'Caja de Jubilaciones y Pensiones de Empl. de Bancos y Afines';

update public.convocante_dim
set
	padre  = 'Congreso Nacional',
	abuelo = 'PODER LEGISLATIVO'
where hijo like 'Cámara%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Cañas Paraguayas S.A.',
	abuelo = 'Cañas Paraguayas S.A.'
where hijo like 'Cañas Paraguayas S.A.%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Comisiones',
	abuelo = 'Comisiones'
where hijo like 'Comisión%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Compañía Paraguaya de Comunicaciones',
	abuelo = 'Compañía Paraguaya de Comunicaciones'
where hijo like 'Compañía Paraguaya de Comunicaciones%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Congreso Nacional',
	abuelo = 'PODER LEGISLATIVO'
where hijo like 'Congreso Nacional%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Consejo de la Magistratura',
	abuelo = 'Consejo de la Magistratura'
where hijo like 'Consejo de la Magistratura%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Contraloría General de la República',
	abuelo = 'Contraloría General de la República'
where hijo like 'Contraloría General de la República%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Corte Suprema de Justicia',
	abuelo = 'Corte Suprema de Justicia'
where hijo like 'Corte Suprema de Justicia%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Crédito Agrícola de Habilitación',
	abuelo = 'Crédito Agrícola de Habilitación'
where hijo like 'Crédito Agrícola de Habilitación%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Defensoría del Pueblo',
	abuelo = 'Defensoría del Pueblo'
where hijo like 'Defensoría del Pueblo%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Direcciones',
	abuelo = 'Direcciones'
where hijo like 'Dirección%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Direcciones',
	abuelo = 'Direcciones'
where hijo like 'Direccion%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Empresa de Servicios Sanitarios del Paraguay',
	abuelo = 'Empresa de Servicios Sanitarios del Paraguays'
where hijo like 'Empresa de Servicios Sanitarios del Paraguay%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Ente Regulador de Servicios Sanitarios',
	abuelo = 'Ente Regulador de Servicios Sanitarios'
where hijo like 'Ente Regulador de Servicios Sanitarios%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Fondos',
	abuelo = 'Fondos'
where hijo like 'Fondo%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Gobernaciones',
	abuelo = 'Gobernaciones'
where hijo like 'Gobierno%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Congreso Nacional',
	abuelo = 'PODER LEGISLATIVO'
where hijo like 'Honorable%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Industria Nacional del Cemento',
	abuelo = 'Industria Nacional del Cemento'
where hijo like 'Industria Nacional del Cemento%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Instituto de Previsión Social',
	abuelo = 'Instituto de Previsión Social'
where hijo like 'Instituto de Previsión Social%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Institutos',
	abuelo = 'Institutos'
where hijo like 'Instituto%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Jurado de Enjuiciamiento de Magistrados',
	abuelo = 'Jurado de Enjuiciamiento de Magistrados'
where hijo like 'Jurado de Enjuiciamiento de Magistrados%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Municipalidades',
	abuelo = 'Municipalidades'
where hijo like 'Loma Plata%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Ministerio de Educación y Cultura',
	abuelo = 'PODER EJECUTIVO'
where hijo like 'Ministerio de Educación y Cultura%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Ministerio de Salud Pública y Bienestar Social',
	abuelo = 'PODER EJECUTIVO'
where hijo like 'Ministerio de Salud Pública y Bienestar Social%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Ministerio de Trabajo, Empleo y Seguridad Social',
	abuelo = 'PODER EJECUTIVO'
where hijo like 'Ministerio de Trabajo, Empleo y Seguridad Social%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Ministerio del Interior',
	abuelo = 'PODER EJECUTIVO'
where hijo like 'Ministerio del Interior%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Ministerios',
	abuelo = 'PODER EJECUTIVO'
where hijo like 'Ministerio%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Municipalidades',
	abuelo = 'Municipalidades'
where hijo like 'Municipalidad%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Petróleos Paraguayos',
	abuelo = 'Petróleos Paraguayos'
where hijo like 'Petróleos Paraguayos%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Secretarias',
	abuelo = 'Secretarias'
where hijo like 'Secretaria%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Secretarias',
	abuelo = 'Secretarias'
where hijo like 'Secretaría%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Servicios Nacionales',
	abuelo = 'Servicios Nacionales'
where hijo like 'Servicio Nacional%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Municipalidades',
	abuelo = 'Municipalidades'
where hijo like 'Tte Irala Fernandez%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Universidad Nacional de Asunción',
	abuelo = 'UNIVERSIDADES NACIONALES'
where hijo like 'Universidad Nacional de Asunción%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Universidades Nacionales',
	abuelo = 'UNIVERSIDADES NACIONALES'
where hijo like 'Universidad%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = 'Vicepresidencia de la República',
	abuelo = 'Vicepresidencia de la República'
where hijo like 'Vicepresidencia de la República%'
and   padre = 'Desconocido';

update public.convocante_dim
set
	padre  = hijo,
	abuelo = hijo
where padre = 'Desconocido';