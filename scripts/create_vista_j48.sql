﻿CREATE OR REPLACE VIEW v_j48 AS 
SELECT 
  similitud_contrato_modificado_fct_tmp.tipo_garantia_oferta, 
  similitud_contrato_modificado_fct_tmp.sistema_adjudicacion, 
  similitud_contrato_modificado_fct_tmp.objeto_licitacion, 
  similitud_contrato_modificado_fct_tmp.forma_pago, 
  similitud_contrato_modificado_fct_tmp.categoria, 
  similitud_contrato_modificado_fct_tmp.tipo_procedimiento, 
  similitud_contrato_modificado_fct_tmp.vigencia_contrato, 
  similitud_contrato_modificado_fct_tmp.dpto_proveedor, 
  similitud_contrato_modificado_fct_tmp.anho_firma, 
  similitud_contrato_modificado_fct_tmp.anho_elec_gral_firma, 
  similitud_contrato_modificado_fct_tmp.anho_elec_mun_firma, 
  similitud_contrato_modificado_fct_tmp.trimestre_firma, 
  similitud_contrato_modificado_fct_tmp.mes_firma, 
  similitud_contrato_modificado_fct_tmp.anho_publicacion, 
  similitud_contrato_modificado_fct_tmp.anho_elec_gral_publicacion, 
  similitud_contrato_modificado_fct_tmp.anho_elec_mun_publicacion, 
  similitud_contrato_modificado_fct_tmp.trimestre_publicacion, 
  similitud_contrato_modificado_fct_tmp.mes_publicacion, 
  similitud_contrato_modificado_fct_tmp.convocante, 
  similitud_contrato_modificado_fct_tmp.monto, 
  similitud_contrato_modificado_fct_tmp.modificado
FROM 
  public.similitud_contrato_modificado_fct_tmp
limit 100 offset 0;

select * from v_j48
where objeto_licitacion = '"Servicios"';