﻿--CREAR UNA TABLA QUE ORDENE Y SETEE EL NUMERO DE REGISTRO
drop sequence next_id cascade;

create	sequence next_id;

drop table v_monto_ordenado cascade;

CREATE table v_monto_ordenado AS 
	SELECT	nextval('next_id')	as nro
		,t.monto_adjudicado 	as monto
		,t.id_contrato 		as id
	FROM 	(SELECT contrato.monto_adjudicado, contrato.id_contrato
		FROM public.contrato 
		order by 1 asc) t;

--CREA LA VISTA DE LOS RANGOS, 10 RANGOS CON APROXIMADAMENTE LA MISMA CANTIDAD DE REGISTROS
create or replace view v_rango_monto as
	select	distinct mo.monto as monto
			,mo.id
		,case	when mo.nro >= 1 and mo.nro <= 10018
			then '"01 Monto entre ' || (select i.monto 
						  from v_monto_ordenado i 
						  where i.nro = 1)
				|| ' y ' || (select f.monto
					     from v_monto_ordenado f
					     where f.nro = 10018) || '"'
			when mo.nro >= 10019 and mo.nro <= 20036
			then '"02 Monto entre ' || (select i.monto 
						  from v_monto_ordenado i 
						  where i.nro = 10019)
				|| ' y ' || (select f.monto
					     from v_monto_ordenado f
					     where f.nro = 20036) || '"'
			when mo.nro >= 20037 and mo.nro <= 30055
			then '"03 Monto entre ' || (select i.monto 
						  from v_monto_ordenado i 
						  where i.nro = 20037)
				|| ' y ' || (select f.monto
					     from v_monto_ordenado f
					     where f.nro = 30055) || '"'
			when mo.nro >= 30056 and mo.nro <= 40074
			then '"04 Monto entre ' || (select i.monto 
						  from v_monto_ordenado i 
						  where i.nro = 30056)
				|| ' y ' || (select f.monto
					     from v_monto_ordenado f
					     where f.nro = 40074) || '"'
			when mo.nro >= 40075 and mo.nro <= 50093
			then '"05 Monto entre ' || (select i.monto 
						  from v_monto_ordenado i 
						  where i.nro = 40075)
				|| ' y ' || (select f.monto
					     from v_monto_ordenado f
					     where f.nro = 50093) || '"'
			when mo.nro >= 50094 and mo.nro <= 60112
			then '"06 Monto entre ' || (select i.monto 
						  from v_monto_ordenado i 
						  where i.nro = 50094)
				|| ' y ' || (select f.monto
					     from v_monto_ordenado f
					     where f.nro = 60112) || '"'
			when mo.nro >= 60113 and mo.nro <= 70131
			then '"07 Monto entre ' || (select i.monto 
						  from v_monto_ordenado i 
						  where i.nro = 60113)
				|| ' y ' || (select f.monto
					     from v_monto_ordenado f
					     where f.nro = 70131) || '"'
			when mo.nro >= 70132 and mo.nro <= 80150
			then '"08 Monto entre ' || (select i.monto 
						  from v_monto_ordenado i 
						  where i.nro = 70132)
				|| ' y ' || (select f.monto
					     from v_monto_ordenado f
					     where f.nro = 80150) || '"'
			when mo.nro >= 80151 and mo.nro <= 90169
			then '"09 Monto entre ' || (select i.monto 
						  from v_monto_ordenado i 
						  where i.nro = 80151)
				|| ' y ' || (select f.monto
					     from v_monto_ordenado f
					     where f.nro = 90169) || '"'
			when mo.nro >= 90170 and mo.nro <= 100182
			then '"10 Monto entre ' || (select i.monto 
						  from v_monto_ordenado i 
						  where i.nro = 90170)
				|| ' y ' || (select f.monto
					     from v_monto_ordenado f
					     where f.nro = 100182) || '"'
		else	'"Desconocido"'
		end	as rango
	from v_monto_ordenado mo;

select * from v_rango_monto order by 1;