﻿drop sequence objeto_licitacion_seq;
CREATE SEQUENCE objeto_licitacion_seq START 1;

insert into public."Objeto_licitacion_dim"
( "Id",
  "Codigo", 
  "Objeto_licitacion")
SELECT
  nextval('objeto_licitacion_seq') as id, 
  v."Codigo", 
  v."Objeto_licitacion"
FROM 
  public.v_objeto_licitacion_dim v
WHERE v."Codigo" not in (select o."Codigo"
			 from public."Objeto_licitacion_dim" o);