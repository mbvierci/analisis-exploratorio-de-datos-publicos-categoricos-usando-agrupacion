CREATE TABLE contrato
(
  id_contrato character varying(100),
  planificacion_id character varying(100),
  convocatoria_id character varying(100),
  adjudicacion_id character varying(100),
  idllamado integer,
  nombre_licitacion character varying(200),
  convocante character varying(200),
  codigo_sicp integer,
  categoria_id integer,
  categoria_codigo integer,
  categoria character varying(200),
  tipo_procedimiento_id integer,
  tipo_procedimiento_codigo character varying(10),
  tipo_procedimiento character varying(200),
  estado_codigo character varying(10),
  estado character varying(20),
  sistema_adjudicacion_codigo character varying(10),
  sistema_adjudicacion character varying(20),
  razon_social character varying(200),
  ruc character varying(20),
  codigo_contratacion character varying(100),
  monto_adjudicado double precision,
  moneda_codigo character varying(10),
  moneda character varying(20),
  numero_contrato character varying(50),
  fecha_firma_contrato timestamp without time zone,
  vigencia_contrato character varying(100),
  anho integer
)
WITH (
  OIDS=FALSE
);

CREATE TABLE origen_modificacion_2
(
  fecha_emision_cc timestamp without time zone,
  fecha_alta timestamp without time zone,
  id_modificacion character varying(200),
  contrato_id character varying(200),
  tipo_codigo character varying(50),
  tipo character varying(50),
  codigo_contratacion character varying(100),
  estado_codigo character varying(50),
  estado character varying(100),
  descripcion character varying(200),
  monto double precision,
  codigo_moneda character varying(50),
  moneda character varying(100),
  vigencia_contrato character varying(100),
  vigencia_contrato_2 character varying(100),
  fecha_contrato timestamp without time zone,
  numero_contrato character varying(200),
  anho integer
)
WITH (
  OIDS=FALSE
);
