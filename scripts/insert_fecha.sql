﻿/** DROPEA LA TABLA VIEJA Fecha_dim **/
--drop table "Fecha_dim";
--drop table fecha_dim;
CREATE TABLE fecha_dim
(
  anho 			integer,
  anho_elec_gral	integer,
  anho_elec_mun		integer,
  trimestre 		integer,	
  mes 			integer,
  mes_descripcion 	character varying,
  dia 			integer,
  fecha 		date NOT NULL,
  CONSTRAINT id_fecha_pk PRIMARY KEY (fecha)
);

/** VISTA INTERMEDIA QUE TRAE LAS FECHAS DISTINTAS Y LAS CONVIERTE A SOLO DATE, SIN TIME 
	Y PARA CARGAR OTROS TIPOS DE FECHAS Y DE NUEVOS ORIGENES, SOLO ES NECESARIO CAMBIAR ACA**/
--drop view v_fecha;
CREATE OR REPLACE VIEW v_fecha AS 
 SELECT DISTINCT date(o.fecha_publicacion) as fecha
 FROM origen_convocatoria o;

--truncate public.fecha_dim;
insert into public.fecha_dim
(anho,
 anho_elec_gral,
 anho_elec_mun,
 trimestre,	
 mes,
 mes_descripcion,
 dia,
 fecha)
select distinct
	date_part('year', v.fecha)				as anho,
	case when (date_part('year', v.fecha)) 
		in (2003, 2008, 2013)
		then 1
	     when v.fecha is null
		then -2
	     else 0
	end							as anho_elec_gral,
	case when (date_part('year', v.fecha)) 
		in (2006, 2010, 2015)
		then 1
	     when v.fecha is null
		then -2
	     else 0
	end							as anho_elec_mun,
	date_part('quarter', v.fecha) 				as trimestre,
	date_part('month', v.fecha) 				as mes,
	case date_part('month', v.fecha)
		when 1 then 'Enero'
		when 2 then 'Febrero'
		when 3 then 'Marzo'
		when 4 then 'Abril'
		when 5 then 'Mayo'
		when 6 then 'Junio'
		when 7 then 'Julio'
		when 8 then 'Agosto'
		when 9 then 'Setiembre'
		when 10 then 'Octubre'
		when 11 then 'Noviembre'
		when 12 then 'Diciembre'
		else 'Desconocido'
	end 							as mes_descripcion,
	date_part('day', v.fecha) 				as dia,
	coalesce(v.fecha, '1900-01-01')				as fecha
from v_fecha v
where v.fecha not in (select distinct f.fecha
				      from public.fecha_dim f)
order by 1;

--VISTA PARA FECHA PUBLICACION
create or replace view v_fecha_publicacion as
select
	fd.anho,
	fd.anho_elec_gral,
	fd.anho_elec_mun,
	fd.trimestre,	
	fd.mes,
	fd.mes_descripcion,
	fd.dia,
	fd.fecha,
	cv.id_llamado
from  fecha_dim fd, v_convocatoria cv
where date(cv.fecha_publicacion) = fd.fecha;

--VISTA QUE CALCULA EL PERIODO ENTRE LA PUBLICACION Y LA FECHA TOPE DE RESPUESTA DE UN LLAMADO
create or replace view v_periodo_public_rpta as
SELECT  
  cv.id_llamado 						as id_llamado,
  (date(cv.fecha_tope_respuesta) - date(cv.fecha_publicacion)) 	as periodo
FROM 
  v_convocatoria cv
  where cv.fecha_tope_respuesta not in ('1899-12-30', '1900-01-01');

--VISTA QUE CALCULA EL PERIODO ENTRE LA PUBLICACION Y LAS FECHAS DE ENTREGA OFERTA O CIERRE DE LA PROPUESTA (CASO DE SUBASTA)
create or replace view v_periodo_pub_ent_cie as
select
	cv.id_llamado									as id_llamado,
	case when cv.fecha_publicacion not in ('1899-12-30', '1900-01-01')
		  and cv.fecha_entrega_oferta not in ('1899-12-30', '1900-01-01')
	     then (date(cv.fecha_entrega_oferta) - date(cv.fecha_publicacion))
	     when cv.fecha_publicacion not in ('1899-12-30', '1900-01-01')
		  and cv.fecha_cierre_propuesta not in ('1899-12-30', '1900-01-01')
	     then (date(cv.fecha_cierre_propuesta) - date(cv.fecha_publicacion))
	     else -2
	end										as periodo
FROM v_convocatoria cv;