﻿--drop sequence estado_seq;
CREATE SEQUENCE sistema_adjudicacion_seq START 1;

insert into public.sistema_adjudicacion_dim
( id,
  codigo, 
  sistema_adjudicacion)
SELECT
  nextval('sistema_adjudicacion_seq') as id, 
  v.codigo, 
  v.sistema_adjudicacion
FROM 
  public.v_sistema_adjudicacion_dim v
where v.codigo not in (select s.codigo
			 from public.sistema_adjudicacion_dim s);