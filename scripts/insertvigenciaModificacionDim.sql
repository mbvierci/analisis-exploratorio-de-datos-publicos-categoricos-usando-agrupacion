CREATE TABLE v_vigencia_modificacion_dim
(
  vigencia_modificacion character varying(100),
  vigencia_modificacion_2 character varying(100),
  valor_vigencia double precision
);

﻿COPY v_vigencia_modificacion_dim FROM '/home/belen/tesis/test.csv' DELIMITER ',' CSV;

CREATE SEQUENCE vigencia_modificacion_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 18850
  CACHE 1;

CREATE TABLE "Vigencia_Modificacion_dim"
(
  "Id" integer NOT NULL,
  vigencia_modificacion character varying(100),
  vigencia_modificacion_2 character varying(100),
  valor_vigencia double precision
  CONSTRAINT "Id_vigencia_modificacion_pk" PRIMARY KEY ("Id" )
);

insert into public."Vigencia_Modificacion_dim"
("Id", vigencia_modificacion, vigencia_modificacion_2, valor_vigencia)
SELECT
  nextval('vigencia_modificacion_seq') as id, 
  vigencia_modificacion,
  vigencia_modificacion_2,
  v.valor_vigencia
FROM 
  v_vigencia_modificacion_dim v
WHERE v.vigencia_modificacion not in (select o.vigencia_modificacion from public."Vigencia_Modificacion_dim" o);
