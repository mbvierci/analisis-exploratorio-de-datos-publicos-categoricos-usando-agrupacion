CREATE TABLE v_provedor_dim
(
  ruc character varying(20),
  razon_social character varying(200)
);

﻿COPY v_provedor_dim FROM '/home/belen/tesis/test.csv' DELIMITER ',' CSV;

insert into public."Proveedor_dim"
( "Id", "Ruc", razon_social)
SELECT
  nextval('proveedor_seq') as id, 
  v.ruc, 
  v.razon_social
FROM 
  v_provedor_dim v
WHERE v.ruc not in (select o."Ruc"
			 from public."Proveedor_dim" o);
