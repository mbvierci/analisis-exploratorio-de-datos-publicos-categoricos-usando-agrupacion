﻿--drop sequence estado_seq;
--CREATE SEQUENCE estado_seq START 1;

insert into public."Estado_dim"
( "Id",
  "Codigo", 
  "Estado")
SELECT
  nextval('estado_seq') as id, 
  v."Codigo", 
  v."Estado"
FROM 
  public.v_estado_dim v
where v."Codigo" not in (select e."Codigo"
			 from public."Estado_dim" e);