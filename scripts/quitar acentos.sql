﻿--Funcion que reemplaza los acentos en todas las columnas
DROP FUNCTION acentos(varchar);
drop table tabla_temporal;
CREATE TABLE tabla_temporal (id serial, columna varchar);
create or replace function acentos(tabla varchar) returns text as 
$$
	declare
		r tabla_temporal%rowtype;
	begin
		--tabla temporal de las columnas de la tabla
		insert into tabla_temporal (columna)
			select column_name as columna
			from information_schema.columns
			where table_name = tabla;

		for r in select * from tabla_temporal where id > 0
			loop
				-- execute 'update ' || tabla ||
-- 					' set ' || r.columna  || ' = regexp_replace(' || r.columna ||', ''á'', ''a'', ''g'');';
-- 				execute 'update ' || tabla ||
-- 					' set ' || r.columna  || ' = regexp_replace(' || r.columna ||', ''é'', ''e'', ''g'');';
-- 				execute 'update ' || tabla ||
-- 					' set ' || r.columna  || ' = regexp_replace(' || r.columna ||', ''í'', ''i'', ''g'');';
-- 				execute 'update ' || tabla ||
-- 					' set ' || r.columna  || ' = regexp_replace(' || r.columna ||', ''ó'', ''o'', ''g'');';
-- 				execute 'update ' || tabla ||
-- 					' set ' || r.columna  || ' = regexp_replace(' || r.columna ||', ''ú'', ''u'', ''g'');';
				execute 'update ' || tabla ||
					' set ' || r.columna  || ' = regexp_replace(' || r.columna ||', '','', '';'', ''g'');';
			end loop;
		
		return 'Fin';
	end;
$$ 
LANGUAGE plpgsql;

select acentos('similitud_contrato_modificado_fct_tmp');
select * from similitud_contrato_modificado_fct_tmp;